from random import shuffle, randrange


colour = ["b", "g", "v", "y", "p", "r", "o"]
count = 0

grid = [[0 for j in range(9)] for i in range(9)]
while count < 3:
    col = randrange(0, 9)
    row = randrange(0, 9)
    shuffle(colour)
    ball = colour[1]
    grid[row].insert(col, ball)
    grid[row].pop(col + 1)
    count += 1
for k in range(9):
    print(grid[k])